-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 12-03-2018 a las 17:49:13
-- Versión del servidor: 5.7.19
-- Versión de PHP: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `games`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `factura`
--

DROP TABLE IF EXISTS `factura`;
CREATE TABLE IF NOT EXISTS `factura` (
  `id` int(15) UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `amount` float(8,2) DEFAULT NULL,
  `sale_date` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `username` (`username`)
) ENGINE=MyISAM AUTO_INCREMENT=44 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `factura`
--

INSERT INTO `factura` (`id`, `username`, `amount`, `sale_date`) VALUES
(43, 'puigsolerh', 156.50, '2018-03-12'),
(42, 'puigsolerh', 25.00, '2018-03-10'),
(41, 'puigsolerh', 25.00, '2018-03-08'),
(40, 'puigsolerh', 25.00, '2018-03-08'),
(39, 'puigsolerh', 25.00, '2018-03-08'),
(34, 'puigsolerh', 61.00, '2018-03-08'),
(38, 'puigsolerh', 25.00, '2018-03-08'),
(37, 'puigsolerh', 25.00, '2018-03-08'),
(36, 'puigsolerh', 25.00, '2018-03-08'),
(35, 'puigsolerh', 150.00, '2018-03-08');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `favs`
--

DROP TABLE IF EXISTS `favs`;
CREATE TABLE IF NOT EXISTS `favs` (
  `username` varchar(200) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `game` varchar(200) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `price` varchar(200) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `favs`
--

INSERT INTO `favs` (`username`, `game`, `price`) VALUES
('puigsolerh', 'zelda', '25.00'),
('puigsolerh', 'mario', '18.50'),
('puigsolerh', 'LoL', '38.1'),
('puigsolerh', 'sonic', '59.00'),
('puigsolerh', 'mario', '18.50'),
('puigsolerh', 'Pokemon', '64.0');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ggames`
--

DROP TABLE IF EXISTS `ggames`;
CREATE TABLE IF NOT EXISTS `ggames` (
  `name` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `code` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `company` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `genere` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `consoles` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `daterent` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `votes` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `opinion` varchar(500) COLLATE utf8_spanish_ci NOT NULL,
  `img` varchar(500) COLLATE utf8_spanish_ci NOT NULL,
  `price` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `ggames`
--

INSERT INTO `ggames` (`name`, `code`, `company`, `genere`, `consoles`, `daterent`, `votes`, `opinion`, `img`, `price`) VALUES
('zelda', '00000000A', 'nintendo', 'adventure', 'PC:Nintendo:', '19/07/1993', '2', 'Aci va una opinio sobre el joc', 'games1.jpg', '25.00'),
('mario', '11111111B', 'sega', 'platform', 'Wii:Nintendo:', '19/07/1993', '8', 'Aci va una opinio sobre el joc', 'games2.jpg', '18.50'),
('fsdaffd', '12345678R', 'company', 'MOBA', 'Xbox:Nintendo:', '14/03/2018', '10', 'wwwwwwwwwwwwwwwwwedsdfgvddaedgvcergs', 'games2.jpg', '4.81'),
('PUBG', '22222222C', 'steam', 'action', 'PC:Xbox:', '19/07/1993', '5', 'Aci va una opinio sobre el joc', 'games3.jpg', '27.00'),
('sonic', '33333333D', 'nintendo', 'adventure', 'PC:Nintendo:', '19/07/1993', '2', 'Aci va una opinio sobre el joc', 'games2.jpg', '59.00'),
('LoL', '44444444E', 'sega', 'platform', 'Wii:Nintendo:', '19/07/1993', '8', 'Aci va una opinio sobre el joc', 'games3.jpg', '38.1'),
('Pokemon', '55555555F', 'steam', 'action', 'PC:Xbox:', '19/07/1993', '5', 'Aci va una opinio sobre el joc', 'games1.jpg', '64.0'),
('nosequeficar', '55556666F', '1DAW', 'MOBA', '', '11/03/2018', '0', 'qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq', 'games2.jpg', '45'),
('sisisisi', '88887777X', 'companyia', 'Shooter', 'Xbox:Nintendo:', '13/03/2018', '3', 'qweeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee', 'games2.jpg', '4');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `linea`
--

DROP TABLE IF EXISTS `linea`;
CREATE TABLE IF NOT EXISTS `linea` (
  `id_lin` int(8) NOT NULL,
  `id_fac` int(8) NOT NULL,
  `game` varchar(200) DEFAULT NULL,
  `price` float(8,2) DEFAULT NULL,
  `qty` int(8) DEFAULT NULL,
  `amount` float(8,2) DEFAULT NULL,
  PRIMARY KEY (`id_lin`,`id_fac`),
  KEY `id_fac` (`id_fac`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `linea`
--

INSERT INTO `linea` (`id_lin`, `id_fac`, `game`, `price`, `qty`, `amount`) VALUES
(5, 43, 'nosequeficar', 45.00, 1, 45.00),
(4, 43, 'sisisisi', 4.00, 1, 4.00),
(3, 43, 'Pokemon', 64.00, 1, 64.00),
(2, 43, 'mario', 18.50, 1, 18.50),
(1, 43, 'zelda', 25.00, 1, 25.00),
(1, 42, 'zelda', 25.00, 1, 25.00),
(1, 41, 'zelda', 25.00, 1, 25.00),
(1, 40, 'zelda', 25.00, 1, 25.00),
(1, 39, 'zelda', 25.00, 1, 25.00),
(1, 38, 'zelda', 25.00, 1, 25.00),
(1, 37, 'zelda', 25.00, 1, 25.00),
(1, 36, 'zelda', 25.00, 1, 25.00),
(3, 35, 'Pokemon', 64.00, 1, 64.00),
(2, 35, 'sonic', 59.00, 1, 59.00),
(1, 35, 'PUBG', 27.00, 1, 27.00),
(1, 34, 'zelda', 25.00, 1, 25.00),
(2, 34, 'FFX', 25.00, 1, 25.00),
(3, 34, 'prova', 11.00, 1, 11.00);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `username` varchar(100) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  PRIMARY KEY (`username`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`username`, `email`, `password`, `type`) VALUES
('puigsolerh', 'hiber98@gmail.com', 'puigsolerh', 'admin'),
('sql_test', 'test@sql.com', 'hola123', 'client'),
('hola123', 'hola@hola.com', 'hola123', 'client'),
('jajajaja', 'a@.com', 'newpassword', 'client'),
('provem', 'email@imeil.com', 'provem', 'client'),
('test', 't@t.com', 'acoesprova', 'client'),
('anemavore', 'email@anem.com', 'contrassenya', 'client'),
('validuser', 'valid@user.es', 'validuser123', 'client'),
('perexemple', 'nose@hotmail.com', 'password', 'client'),
('hiber1', 'hiber@hiber.com', 'passworrrrd', 'client'),
('aaaaaaa', 'aaaa@a.com', 'aaaaaaa', 'client');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
