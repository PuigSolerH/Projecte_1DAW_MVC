<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
        <head>
                <meta charset="utf-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <title>Flew — Free HTML5 Bootstrap Website Template by FreeHTML5.co</title>
                <meta name="viewport" content="width=device-width, initial-scale=1">
                <meta name="description" content="Free HTML5 Website Template by FreeHTML5.co" />
                <meta name="keywords" content="free html5, free template, free bootstrap, free website template, html5, css3, mobile first, responsive" />
                <meta name="author" content="FreeHTML5.co" />

          <!--
                //////////////////////////////////////////////////////

                FREE HTML5 TEMPLATE
                DESIGNED & DEVELOPED by FreeHTML5.co

                Website:                http://freehtml5.co/
                Email:                  info@freehtml5.co
                Twitter:                http://twitter.com/fh5co
                Facebook:               https://www.facebook.com/fh5co

                //////////////////////////////////////////////////////
                 -->

                <!-- Facebook and Twitter integration -->
                <meta property="og:title" content=""/>
                <meta property="og:image" content=""/>
                <meta property="og:url" content=""/>
                <meta property="og:site_name" content=""/>
                <meta property="og:description" content=""/>
                <meta name="twitter:title" content="" />
                <meta name="twitter:image" content="" />
                <meta name="twitter:url" content="" />
                <meta name="twitter:card" content="" />

                <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
                <link rel="shortcut icon" href="favicon.ico">

                <link href="https://fonts.googleapis.com/css?family=Raleway:200,300,400,700" rel="stylesheet">

                <!-- Animate.css -->
                <link rel="stylesheet" href="assets/css/animate.css">
                <!-- Icomoon Icon Fonts-->
                <link rel="stylesheet" href="assets/css/icomoon.css">
                <!-- Bootstrap  -->
                <link rel="stylesheet" href="assets/css/bootstrap.css">
                <!-- Flexslider  -->
                <link rel="stylesheet" href="assets/css/flexslider.css">
                <!-- Owl Carousel  -->
                <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
                <link rel="stylesheet" href="assets/css/owl.theme.default.min.css">
                <!-- Theme style  -->
                <link rel="stylesheet" href="assets/css/style.css">

                <!-- Modernizr JS -->
                <script src="assets/js/modernizr-2.6.2.min.js"></script>
                <!-- FOR IE9 below -->
                <!--[if lt IE 9]>
                <script src="js/respond.min.js"></script>
                <![endif]-->
                <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
                <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
                <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
                <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
                <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">
                <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> -->
                <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> -->
                <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->
                <script type="text/javascript">
                $(function() {
          		    $('#daterent').datepicker({
          		      dateFormat: 'dd/mm/yy',
          		      changeMonth: true,
          		      changeYear: true,
          		      yearRange: '0:+2',
          		      minDate: 0,
          		      //onSelect: function(selectedDate) {
          		        //alert(selectedDate);
          		      //}
          		    });
          		  });
                </script>
                <script src="module/games/model/validate_games.js"></script>
                <script src="module/login/model/validate_register.js"></script>
                <script src="module/login/model/validate_recover.js"></script>
                <script src="module/login/model/validate_login.js"></script>
                <script src="module/inicio/js/main.js"></script>
                <script src="module/API/model/model_api.js"></script>
                <script src="module/cart/model/cart.js"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.min.js"></script>
                <script src="module/cart/model/pdf.js"></script>
                <script src="module/like/model/like.js"></script>

        </head>
        <body>
