<footer id="fh5co-footer" role="contentinfo">
  <div class="container">
    <div class="col-md-3 col-sm-12 col-sm-push-0 col-xs-12 col-xs-push-0">
      <h3>About Us</h3>
      <ul class="fh5co-social">
    <li><a href=""><?php echo $texts['contact_number']?></a></li>
    <li><a href=""><?php echo $texts['email']?></a></li>
    <li><a href=""><?php echo $texts['address']?></a></li>
  </ul>
    </div>

  <div class="col-md-2 col-md-push-1 col-sm-12 col-sm-push-0 col-xs-12 col-xs-push-0">
    <h3>Follow Us</h3>
    <ul class="fh5co-social">
      <li><a href="#"><i class="icon-twitter"></i></a>
      <a href="#"><i class="icon-facebook"></i></a>
      <a href="#"><i class="icon-google-plus"></i></a>
      <a href="#"><i class="icon-instagram"></i></a></li>
    </ul>
  </div>
  <div class="col-md-12 fh5co-copyright text-center">
    <p>&copy; 2016 Free HTML5 template. All Rights Reserved. <span>Designed with <i class="icon-heart"></i> by <a href="http://freehtml5.co/" target="_blank">FreeHTML5.co</a> Demo Images by <a href="http://unsplash.com/" target="_blank">Unsplash</a></span></p>
  </div>
</div>
</footer>
