<header id="fh5co-header" role="banner">
  <div class="container">
    <div class="header-inner">
      <h1><a href="index.php">Games</a></h1>
      <nav role="navigation">
  <ul>
    <li><a href="index.php?page=controller_dummies&op=list"><span class="glyphicon glyphicon-home"></span> <?php echo $texts['index']?></a></li>
	<li><a href="index.php?page=controller_games&op=list"><?php echo $texts['games']?></a></li>
  <li><a href="index.php?page=contactus"><span class="glyphicon glyphicon-phone-alt"></span> <?php echo $texts['contact']?></a></li>
  <li><a href="index.php?page=api"><span class="glyphicon glyphicon-film"></span> Twitch</a></li>
  <li><a href="index.php?page=controller_cart&op=cart" ><span class="glyphicon glyphicon-shopping-cart"></span> MY CART</a></li>
  <li><a href="index.php?page=controller_like&op=list"><span class="glyphicon glyphicon-user"></span><?php echo $_SESSION['user_logged']->username?></li>
  <li><a href="index.php?page=controller_login&op=logout"><span class="glyphicon glyphicon-log-out"></span>LOG OUT</a></li>
    |
  <li><form method="POST">
    	<select name="lang">
    		<option value="es">Español</option>
    		<option value="en">English</option>
    		<option value="va">Valencià</option>
    	</select>
	 <button class="btn btn-xs btn-default" type="submit"><span class="glyphicon glyphicon-globe"></span></button>
	</form></li>
</ul>
</nav>
</div>
</header>
