<?php

$texts = array(
  //MENU
  'index' => 'Inicio',
  'games' => 'Juegos',
  'services' => 'Servicios',
  'about_us' => 'Sobre nosotros',
  'contact' => 'Contacta',

  //FOOTER
  'contact_number' => 'Numero de contacto',
  'email' => 'Correo Electronico',
  'address' => 'Direccion',

  //BUTTONS
  'back' => 'Volver',
  'accept' => 'Aceptar',
  'cancel' => 'Cancelar',

  //TITTLES
  'list' => 'Lista de juegos',
  'read' => 'Informacion',
  'create' => 'Crear un nuevo juego',
  'delete' => 'Quieres eliminar el juego',
  'update' => 'Actualizar',


  //INFO_GAME
  'name' => 'Nombre',
  'code' => 'Codigo',
  'company' => 'Compañia',
  'genere' => 'Genero',
  'consoles' => 'Consoles',
  'daterent' => 'Fecha de alquiler',
  'datereturn' => 'Fecha de devolucion',
  'password' => 'Contraseña',
  'password2' => 'Repite la contraseña',
  'votes' => 'Votos',
  'opinion' => 'Opinion',
  'img' => 'Imagen',
  'price' => 'Precio',

  //OTHER
  'action' => 'Accion',
);
