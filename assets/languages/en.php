<?php
$texts = array(
  //MENU
  'index' => 'Index',
  'games' => 'Games',
  'services' => 'Services',
  'about_us' => 'About Us',
  'contact' => 'Contact',

  //FOOTER
  'contact_number' => 'Contact number',
  'email' => 'Email',
  'address' => 'Address',

  //BUTTONS
  'back' => 'Back',
  'accept' => 'Accept',
  'cancel' => 'Cancel',

  //TITTLES
  'list' => 'Games List',
  'read' => 'Info',
  'create' => 'Create a new game',
  'delete' => 'Do you want to delete the game',
  'update' => 'Update',


  //INFO_GAME
  'name' => 'Name',
  'code' => 'Code',
  'company' => 'Company',
  'genere' => 'Genere',
  'consoles' => 'Consolas',
  'daterent' => 'Renting date',
  'datereturn' => 'Returning date',
  'password' => 'Password',
  'password2' => 'Repeat the password',
  'votes' => 'Votes',
  'opinion' => 'Opinion',
  'img' => 'Image',
  'price' => 'Price',

  //OTHER
  'action' => 'Action',
);
