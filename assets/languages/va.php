<?php
$texts = array(
  //MENU
  'index' => 'Inici',
  'games' => 'Jocs',
  'services' => 'Serveis',
  'about_us' => 'Sobre nosaltres',
  'contact' => 'Contacta',

  //FOOTER
  'contact_number' => 'Nombre de contacte',
  'email' => 'Correu electronic',
  'address' => 'Direccio',

  //BUTTONS
  'back' => 'Enrere',
  'accept' => 'Acceptar',
  'cancel' => 'Cancelar',

  //TITTLES
  'list' => 'Llista de jocs',
  'read' => 'Informacio',
  'create' => 'Crear un nou joc',
  'delete' => 'Vols el·liminar el joc',
  'update' => 'Actualitzar',


  //INFO_GAME
  'name' => 'Nom',
  'code' => 'Codi',
  'company' => 'Companyia',
  'genere' => 'Genere',
  'consoles' => 'Consoles',
  'daterent' => 'Data de alquiler',
  'datereturn' => 'Data de devolucio',
  'password' => 'Contrassenya',
  'password2' => 'Repeteix la contrassenya',
  'votes' => 'Vots',
  'opinion' => 'Opinio',
  'img' => 'Imatge',
  'price' => 'Preu',

  //OTHER
  'action' => 'Accio',
);
