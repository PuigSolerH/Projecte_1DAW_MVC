Projecte realitzat per Hibernon Puig Soler

Al HOME de l'aplicació es pot trobar amb
  - Vista inicial de la pàgina si es polsa sobre la paraula Games on es troben els 'Dependent Dropdowns' per JSON i un 'Autocomplete' amb un botó a la dreta per lllistar-ne el contingut.
  - INICIO: Conté un llistat de tots els productes amb informació i dos botons. Un per saber-ne més i l'altre per afegir el producte a la cistella.
  - JUEGOS: Ací sols pot accedir l'administrador/administradors de la pàgina i és on es realitzen les funcions del CRUD.
  - CONTACTA: Aquesta vista conté un mapa amb una ubicacó predeterminada.
  - TWITCH: Són dades recollides de l'API de Twitch amb informació sobre l'estat dels canals de videojocs més vistos de la platafroma amb accés al contingut de cada canal.
  - MY CART: Cistella on es poden veure tots els productes afegits i és pot realitzar un 'checkout' sobre ells.
  - LOG IN: Vista d'inici de sessió per usuaris ja registrats amb un botó també per poder-ne canviar la contrassenya d'alguns d'aquests.
  - REGISTER: Vista per registrar un usuari.
  - A la dreta del tot es troba un desplegable on es pot sel·leccionar un idioma i, al polsar sobre el botó de la seua dreta, la pàgina canviarà a l'idioma sel·leccionat.
  - Si la sessió ja esta iniciada l'opció register serà canviada per la de LOG OUT on l'usuari podrà tancar la seua sessió.
  - Si la sessió ja esta iniciada l'opció LOG IN serà canviada per un text amb el nom de l'usuari el qual és recollit per la variable $_SESSION.

Millores:
  - 'Recover password' per a cada usuari.
  - Expiració de sessió de l'usuari.
  - Validació PHP (al Create).
  - Si la sessió no està iniciada i es polsa sobre l'opció CART del menú, l'uauari serà redirigit al LOG IN.
  - PDFJS dels productes sel·leccionats a la cistella.
