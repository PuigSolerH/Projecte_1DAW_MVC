function validate_register() {
	event.preventDefault();
    if (document.formR.s_username.value.length===0 ){
	    document.getElementById('e_username').innerHTML = "Introduce the username";
	    document.formR.s_username.focus();
	    return 0;
    }else if(document.formR.s_username.value !== ""){
		var username_regex = /^[a-zA-Z0-9]{6,16}$/;
		var validUsername = document.formR.s_username.value.match(username_regex);
		if(validUsername == null){
				alert("Invalid username");
				// document.formR.username.focus();
				return 0;
		}
		}
    document.getElementById('e_username').innerHTML = "";

    if (document.formR.s_email.value.length===0){
	    document.getElementById('e_email').innerHTML = "Introduce the email";
	    document.formR.s_email.focus();
	    return 0;
    }else if(document.formR.s_email.value !== ""){
		var email_regex = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
		var validEmail = document.formR.s_email.value.match(email_regex);
		if(validEmail == null){
				alert("Invalid email");
				// document.formR.username.focus();
				return 0;
		}
		}
    document.getElementById('e_email').innerHTML = "";

    if (document.formR.s_password.value.length===0){
      document.getElementById('e_password').innerHTML = "Introduce a password";
      document.formR.s_password.focus();
      return 0;
    }else if(document.formR.s_password.value !== ""){
		var password_regex = /^[a-zA-Z0-9!@#$%^&*]{6,16}$/;
		var validPassword = document.formR.s_password.value.match(password_regex);
		if(validPassword == null){
				alert("Invalid password");
				// document.formR.username.focus();
				return 0;
		}
		}
    document.getElementById('e_password').innerHTML = "";

		document.formR.submit();
		document.formR.action="index.php?page=controller_login&op=register";

}
