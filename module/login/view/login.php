<div id="contenido" class="container">
  <div class="omb_login">
  <h3 class="omb_authTitle">LOG IN</h3>
    <div class="row omb_row-sm-offset-3">
    	<div class="col-xs-12 col-sm-6">
        <div id="form_results"></div>
        <form autocomplete="off" class="omb_loginForm" method="post" name="formL" id="formL">
          <p>
            <label for="username">USERNAME</label>
            <input name="l_username" type="text" id="l_username" class="form-control" placeholder="Insert a username" />
          </p>
          <p>
          <label for="password"><?php echo $texts['password']?></label>
          <input name="l_password" id="l_password" type="password" class="form-control" placeholder="Introduce the password"/>
          </p>
            <input class="btn btn-lg btn-primary btn-block" type="button" id="login" name="login" value="Log In"/>
          </br>
            <a href="index.php?page=controller_login&op=recover" class="btn btn-warning"><span class="glyphicon glyphicon-asterisk"></span>¿HAS OLVIDADO LA CONTRASEÑA?</a>
                    <!--<td><input type="submit" name="create" id="create"/></td>-->
            <!-- <td><a class="btn btn-primary btn-outline" href="index.php?page=controller_login&op=signin">SIGNIN</a></td> -->
        </form>
      </div>
    </div>
  </div>
</div>
