<div id="contenido" class="container">
  <div class="omb_login">
  <h3 class="omb_authTitle">CHANGE THE PASSWORD</h3>
    <div class="row omb_row-sm-offset-3">
    	<div class="col-xs-12 col-sm-6">
        <div id="form_results"></div>
        <form autocomplete="off" class="omb_loginForm" method="post" name="formRec" id="formRec">
          <p>
            <label for="email">EMAIL</label>
            <input name="r_email" type="text" id="r_email" class="form-control" placeholder="Insert the email" value="" />
          </p>
          <p>
          <label for="password">NUEVA CONTRASEÑA</label>
          <input name="new_password" id="new_password" type="password" class="form-control" placeholder="Introduce the new password" value="" />
          </p>
            <input class="btn btn-lg btn-warning btn-block" type="submit" id="recover" name="recover" value="Recover" />
          </br>
        </form>
      </div>
    </div>
  </div>
</div>
