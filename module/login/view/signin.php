<div id="contenido" class="container">
  <div class="omb_login">
  <h3 class="omb_authTitle">REGISTER</h3>
  <div class="row omb_row-sm-offset-3">
    <div class="col-xs-12 col-sm-6">
    <form autocomplete="off" method="post" class="omb_loginForm" name="formR" id="formR" action="index.php?page=controller_login&op=signin">
      <!-- action="index.php?page=controller_login&op=signin" -->
      <p>
        <label for="username">USERNAME</label>
        <input name="s_username" type="text" id="s_username" class="form-control" placeholder="Insert a username" value=""/>
        <span id="e_username" class="styerror">
        <?php
            // echo "$error_username";
        ?>
        </span>
      </p>
      <p>
        <label for="email">EMAIL</label>
        <input name="s_email" type="text" id="s_email" class="form-control" placeholder="Insert the email" value=""/>
        <span id="e_email" class="styerror">
          <?php
              // echo "$error_email";
          ?>
        </span>
      </p>
      <p>
      <label for="password"><?php echo $texts['password']?></label>
      <input name="s_password" id="s_password" type="password" class="form-control" placeholder="Introduce the password" value=""/>
      <span id="e_password" class="styerror"></span>
      </p>
        <input class="btn btn-lg btn-primary btn-block" type="button" name="signin" value="SIGN IN" id="signin" onclick="return validate_register();"/>
                <!--<td><input type="submit" name="create" id="create"/></td>-->
    </form>
  </div>
</div>
</div>
</div>
