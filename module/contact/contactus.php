<h3>Contactanos</h3>
<html>
  <head>
    <style>
      #map {
        width: 100%;
        height: 600px;
        background-color: grey;
      }
    </style>
  </head>
  <body>
    <div id="map"></div>
    <script>
      function initMap() {
        var uluru = {lat: 38.8151804, lng: -0.606651};
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 10,
          center: uluru
        });
        var marker = new google.maps.Marker({
          position: uluru,
          map: map
        });
      }
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCTtuaWQrIouWDRHSvecgJK5LxrcHqz4_Q&callback=initMap">
    </script>
  </body>
</html>
