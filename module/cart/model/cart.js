var cart = [];
$(function () {
    if (localStorage.cart){
        cart = JSON.parse(localStorage.cart);
        console.log(cart);
        showCart();
    }
});

$(document).ready(function() {
  $('button#btnAdd').click(function(event){
		// alert("HOLA");
    event.preventDefault(); //prevent default action
    var name = $(this).find(".title").attr('id');
    // alert(name);
    var genere = $(this).find(".genere").attr('id');
    var price = $(this).find(".price").attr('id');
    var qty = "1";
    var amount = (qty*price);
    // var qty = document.getElementById(".qty").value;
    // console.log(qty);
    localStorage.setItem("Game", name);
    localStorage.setItem("Genere", genere);
    localStorage.setItem("Price", price);

    for (var i in cart) {
      if(cart[i].Game == name){
          // cart[i].Qty = qty;
          showCart();
          saveCart();
          return;
      }
    }

    var item = { Game: name, Genere: genere, Price: price, Qty: qty, Amount: amount };
    cart.push(item);
    saveCart();
    showCart();

    // alert("AÑADIDO AL CARRITO");
  });
});

// $(document).ready(function() {
//   $('button#slct_qty').click(function(event){
//     // alert("HOLA");
//     // var qty = $(this).find(".qty").attr('id');
//     var qty = $("[type=number]").val();
//     if (qty < 1){
//       alert ("No puedes seleccionar productos por un valor menor a 1");
//       die();
//     }
//     // alert(qty);
//     var name = localStorage.getItem("Game");
//     var genere = localStorage.getItem("Genere");
//     var price = localStorage.getItem("Price");
//     // alert(price);
//     var amount = (qty*price);
//     // alert(amount);
//
//     for (var i in cart) {
//       if(cart[i].Game == name){
//           cart[i].Qty = qty;
//           cart[i].Price = price;
//           cart[i].Amount = amount;
//           showCart();
//           saveCart();
//           return;
//       }
//     }
//     // cart.push(qty,amount);
//     // deleteItem(item);
//     var item = { Game: name, Genere: genere, Price: price, Qty: qty, Amount: amount};
//     cart.push(item);
//     saveCart();
//     showCart();
//   });
// });

function deleteItem(index){
    cart.splice(index,1); // delete item at index
    showCart();
    saveCart();
}

// function deleteCart(){
//   var cart = [];
//   showCart();
//   saveCart();
// }

function saveCart() {
    if ( window.localStorage){
        localStorage.cart = JSON.stringify(cart);
    }
}

function showCart() {
  if (cart.length == 0) {
      $("#cart").css("visibility", "hidden");
      return;
  }

  $("#cart").css("visibility", "visible");
  $("#cartBody").empty();
  for (var i in cart) {
      var item = cart[i];
      var row = "<tr><td>" + item.Game + "</td><td>" +
                   item.Genere + "</td><td>" + item.Price + "</td><td>"
                   + item.Qty + "</td><td>"
                   + item.Amount + "</td><td>"
                   + "<button class='btn btn-xs btn-danger' onclick='deleteItem(" + i + ")'><span class='glyphicon glyphicon-remove'></span></button></td></tr>";
      $("#cartBody").append(row);
    }
}

$(document).ready(function() {
  $('button#checkout').click(function(event){
    // alert("HOLA");
    var json_cart = JSON.stringify(cart);
    // console.log(json_cart);
    $.ajax({
      type: "POST",
      url: "module/cart/controller/controller_cart.php?op=checkout",
      data: {data: json_cart},
      success: function(data, status) {
        console.log(data);
        // console.log(status);
        // deleteCart();
        // if(data==1){
        //   $("#results").html("SUCCESS");
        //   // alert("You have been logged");
        //   location.href='index.php?page=controller_dummues&op=list';
        // }else if(data==0){
        //   $("#results").html('ERROR');
        //   location.href='index.php?page=controller_cart&op=cart';
        // }
        // if(data=0){
        //
        // }else if(data=1){
        //
        // }
      }
    });
  });
});
// + <input id='qty' type='number' min='1'></input><button id='slct_qty' class='btn btn-xs btn-success' type='button' onClick='location.reload();'><span class='glyphicon glyphicon-ok'></span></button>"
