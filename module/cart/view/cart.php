<div id="results">
  <table id="cart" border="1" style="visibility:hidden; width:100%">
       <thead>
            <tr>
                <th>Name</th>
                <th>Genere</th>
                <th>Price</th>
                <th>Quantity</th>
                <th>Total</th>
                <th></th>
           </tr>
       </thead>
       <tbody id="cartBody"></tbody>
  </table>
</div>
<button type="button" id="pdf" class="btn btn-default" onclick="javascript:demoFromHTML();"><span class="glyphicon glyphicon-download"></span> PDF</button>
</br></br>
<button id="checkout" class="btn btn-block btn-success" type="button"><span class="glyphicon glyphicon-usd"></span></button>
