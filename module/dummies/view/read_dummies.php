<div id="contenido" class="datagrid">
  <h3><?php echo $texts['read']?></h3>
    <p>
      <table border='2'>
          <tr>
              <td><?php echo $texts['name']?>: </td>
              <td>
                  <?php
                      echo $dummies['name'];
                  ?>
              </td>
          </tr>

          <tr>
              <td><?php echo $texts['code']?>: </td>
              <td>
                  <?php
                      echo $dummies['code'];
                  ?>
              </td>
          </tr>

          <tr>
              <td><?php echo $texts['company']?>: </td>
              <td>
                  <?php
                      echo $dummies['company'];
                  ?>
              </td>
          </tr>

          <tr>
              <td><?php echo $texts['price']?>: </td>
              <td>
                  <?php
                      echo $dummies['price'];
                  ?>
              </td>
          </tr>

          <tr>
              <td><?php echo $texts['genere']?>: </td>
              <td>
                  <?php
                      echo $dummies['genere'];
                  ?>
              </td>
          </tr>

          <tr>
              <td><?php echo $texts['consoles']?>: </td>
              <td>
                  <?php
                      echo $dummies['consoles'];
                  ?>
              </td>
          </tr>

          <tr>
              <td><?php echo $texts['daterent']?>: </td>
              <td>
                  <?php
                      echo $dummies['daterent'];
                  ?>
              </td>
          </tr>

          <tr>
              <td><?php echo $texts['votes']?>: </td>
              <td>
                  <?php
                      echo $dummies['votes'];
                  ?>
              </td>
          </tr>

          <tr>
              <td><?php echo $texts['opinion']?>: </td>
              <td>
                  <?php
                      echo $dummies['opinion'];
                  ?>
              </td>
          </tr>

          <!-- <tr>
              <td><?php echo $texts['img']?>: </td>
              <td>
                  <?php
                      echo $dummies['img'];
                  ?>
              </td>
          </tr> -->
      </table>
</p>
<p><a class="btn btn-primary btn-outlines" href="index.php?page=controller_dummies&op=list"><?php echo $texts['back']?></a></p>
</div>
