<div id="fh5co-work-section" class="fh5co-light-grey-section">
  <!-- <h2 style="text-align: center;">Select dependientes usando JSON</h2> -->
    <div class="container">
    	<div class="row">
    			<h3><?php echo $texts['list']?></h3>
      </div>
                <?php
                    if ($rdo->num_rows === 0){
                        echo '<tr>';
                        echo '<td align="center"  colspan="3">NO GAMES AVAILABLE</td>';
                        echo '</tr>';
                    }else{
                        foreach ($rdo as $row) {
                          echo '<div class="col-md-4">';
                       		echo '<div class="item-grid text-center">';
                          echo '<div class="image" style="background-image: url(assets/images/'.$row['img'].'") alt="">';
                          echo '</div>';
                          echo '<div class="v-align">';
              						echo '<div class="v-align-middle">';
                    	   	echo '<h3 class="title">'.$row['name'].'</h3>';
                    	   	echo '<h5 class="category">'.$row['code'].'</h5>';
                    	   	echo '<h5 class="category">'.$row['price'].'</h5>';
                    	   	echo '<a class="btn btn-primary btn-outline" href="index.php?page=controller_dummies&op=read&id='.$row['code'].'">Read</a>';
                          echo '<button id="btnAdd" class="btn btn-primary btn-outline"><span class="glyphicon glyphicon-shopping-cart"></span>
                                <div class="title" id="'.$row['name'].'" style="display: none;"></div>
                                <div class="genere" id="'.$row['genere'].'" style="display: none;"></div>
                                <div class="price" id="'.$row['price'].'" style="display: none;"></div></button>';
                          echo '<button id="btnLike" class="btn btn-primary btn-outline">
                                <div class="title" id="'.$row['name'].'" style="display: none;"></div>
                                <div class="genere" id="'.$row['genere'].'" style="display: none;"></div>
                                <div class="price" id="'.$row['price'].'" style="display: none;"></div>Like</button>';
                    	   	echo '</div>';
                          echo '</div>';
                          echo '</div>';
                          echo '</div>';
                        }
                    }
                ?>
    </div>
</div>
