$(function() {

  var follower = ["ESL_SC2", "imaqtpie", "TSM_Myth", "dakotaz", "OgamingSC2", "ThijsHS", "StarLadder5", "alexelcapo", "NightBlue3", "Evangelion0", "FeelinkHS", "Wismichu", "LVPes", "shroud"];

  for (var i = 0; i < follower.length; i++) {

    $.ajax({
      type: 'GET',
      url: 'https://api.twitch.tv/kraken/streams/' + follower[i], //change this to test active
      headers: {
        'client-ID': 'wu6qm10lt8jb5mxhhq1uw227hoz8e8'
      },
      success: function(dataI) {

        var name = dataI._links.self.slice(37)
        // console.log(dataI);

        if (dataI.stream === null) {
          $('#content').append('<div class="col-md-4">' +
                        '<div class="item-grid text-center">' +
                        '<div class="image"><img src="assets/images/offline.jpg"></img>' +
                        '</div>' +
                        '<div class="v-align">' +
                        '<div class="v-align-middle">' +
                        '<h3 class="title">' + name + '</h3>' +
                        '<h5 class="category">IS OFFLINE</h5>' +
                        '<h5 class="category">N/A</h5>' +
                        '<a class="btn btn-danger btn-outline" href="https://www.twitch.tv/' + name + '"><span class="glyphicon glyphicon-info-sign"></span> INFO</a>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>'
          );
          // $('#status').append(' is offline<br>');
          // $('#user').append('<a target="blank" href="https://www.twitch.tv/' + name + '">' + name + '</a><br>');
          // $('#game').append('N/A<br>');
          // $('#preview').append('N/A<br>');
        } else {
          $('#content').append('<div class="col-md-4">' +
                        '<div class="item-grid text-center">' +
                        '<div class="image"><img src="'+ dataI.stream.preview.medium + '"></img>' +
                        '</div>' +
                        '<div class="v-align">' +
                        '<div class="v-align-middle">' +
                        '<h3 class="title"><span>' + name + '</span></h3>' +
                        '<h5 class="category">IS ONLINE</h5>' +
                        '<h5 class="category">' + dataI.stream.game + '</h5>' +
                        '<a class="btn btn-success btn-outline" href="https://www.twitch.tv/' + name + '"><span class="glyphicon glyphicon-play-circle"></span> PLAY</a>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>'
          );
          // $('#status').append(' is ONLINE!<br>');
          // $('#user').append('<a target="blank" href="https://www.twitch.tv/' + name + '">' + name + '</a><br>')
          // $('#game').append(dataI.stream.game + '<br>');
          // $('#preview').append('<img src="'+ dataI.stream.preview.small + '"></img></br>');
        }
      },
      // error: function(err) {
      //   alert("Error: One or more users is no longer avaialble");
      // }
    });

  }

})
