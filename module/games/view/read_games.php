    <div id="contenido" class="datagrid">
      <h3><?php echo $texts['read']?></h3>
        <p>
          <table border='2'>
              <tr>
                  <td><?php echo $texts['name']?>: </td>
                  <td>
                      <?php
                          echo $games['name'];
                      ?>
                  </td>
              </tr>

              <tr>
                  <td><?php echo $texts['code']?>: </td>
                  <td>
                      <?php
                          echo $games['code'];
                      ?>
                  </td>
              </tr>

              <tr>
                  <td><?php echo $texts['company']?>: </td>
                  <td>
                      <?php
                          echo $games['company'];
                      ?>
                  </td>
              </tr>

              <tr>
                  <td><?php echo $texts['price']?>: </td>
                  <td>
                      <?php
                          echo $games['price'];
                      ?>
                  </td>
              </tr>

              <tr>
                  <td><?php echo $texts['genere']?>: </td>
                  <td>
                      <?php
                          echo $games['genere'];
                      ?>
                  </td>
              </tr>

              <tr>
                  <td><?php echo $texts['consoles']?>: </td>
                  <td>
                      <?php
                          echo $games['consoles'];
                      ?>
                  </td>
              </tr>

              <tr>
                  <td><?php echo $texts['daterent']?>: </td>
                  <td>
                      <?php
                          echo $games['daterent'];
                      ?>
                  </td>
              </tr>

              <tr>
                  <td><?php echo $texts['votes']?>: </td>
                  <td>
                      <?php
                          echo $games['votes'];
                      ?>
                  </td>
              </tr>

              <tr>
                  <td><?php echo $texts['opinion']?>: </td>
                  <td>
                      <?php
                          echo $games['opinion'];
                      ?>
                  </td>
              </tr>

              <tr>
                  <td><?php echo $texts['img']?>: </td>
                  <td>
                      <?php
                          echo $games['img'];
                      ?>
                  </td>
              </tr>
          </table>
    </p>
    <p><a class="btn btn-primary btn-outline with-arrow" href="index.php?page=controller_games&op=list"><?php echo $texts['back']?></a></p>
</div>
