<div id="fh5co-work-section" class="fh5co-light-grey-section">
    <div class="container">
    	<div class="row">
    			<h3><?php echo $texts['list']?></h3>
                </br></br>
    	</div>
    	<div class="datagrid">
    		<p><a class="btn btn-primary" href="index.php?page=controller_games&op=create"><?php echo $texts['create']?></a></p>
    		<table id=list  class="table table-striped table-bordered" >
          <thead>
                <tr>
                    <td width=125><b><?php echo $texts['name']?></b></td>
                    <td width=125><b><?php echo $texts['code']?></b></td>
                    <td width=125><b><?php echo $texts['genere']?></b></td>
                    <td width=350><b><?php echo $texts['action']?></b></td>
                </tr>
          </thead>
          <tbody>
                <?php
                    if ($rdo->num_rows === 0){
                        echo '<tr>';
                        echo '<td align="center"  colspan="3">NO GAMES AVAILABLE</td>';
                        echo '</tr>';
                    }else{
                        foreach ($rdo as $row) {
                       		echo '<tr>';
                    	   	echo '<td width=125>'. $row['name'] . '</td>';
                    	   	echo '<td width=125>'. $row['code'] . '</td>';
                    	   	echo '<td width=125>'. $row['genere'] . '</td>';
                    	   	echo '<td width=350>';
                    	   	// echo '<a class="button-success pure-button openBtn2" data-toggle="modal" data-target="#myModal" href="index.php?page=controller_games&op=read&id='.$row['name'].'">Read</a>';
                          print ("<div class='list_game btn btn-primary btn-outline' id='".$row['code']."'>Read</div>");  //READ
                          echo '&nbsp;';
                    	   	echo '<a class="btn btn-primary btn-outline" href="index.php?page=controller_games&op=update&id='.$row['code'].'">Update</a>';
                    	   	echo '&nbsp;';
                    	   	echo '<a class="btn btn-primary btn-outline" href="index.php?page=controller_games&op=delete&id='.$row['code'].'">Delete</a>';
                    	   	echo '</td>';
                    	   	echo '</tr>';
                        }
                    }
                ?>
            </tbody>
          </table>
    	</div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('#list').DataTable();
    } );
</script>
<!-- modal window -->
<section id="myModal">
    <div id="details_games" hidden>
        <div id="details">
            <div id="container">
                Name: <div id="name"></div></br>
                Code: <div id="code"></div></br>
                Company: <div id="company"></div></br>
                Genere: <div id="genere"></div></br>
                Consoles: <div id="consoles"></div></br>
                Price(€): <div id="price"></div></br>
                Daterent: <div id="daterent"></div></br>
                Votes: <div id="votes"></div></br>
                Opinion: <div id="opinion"></div></br>
                <!-- Image: <div id="img"></div></br> -->
            </div>
        </div>
    </div>
</section>
