<div id="contenido" class="datagrid">
  <h3><?php echo $texts['create']?></h3>
</br>
    <form autocomplete="off" method="post" name="formulario" id="formulario" action="index.php?page=controller_games&op=create">
      <?php
  		if(isset($error)){
        // print_r($error);
  			print ("<span CLASS='styerror'>" .$error . "</span><br/>");
  		}?>
      <p>
        <label for="name"><?php echo $texts['name']?></label>
        <input name="name" type="text" id="name" placeholder="Insert your name" value="<?php echo $_POST?$_POST['name']:"";?>" />
        <span id="e_name" class="styerror">
        </span>
      </p>
      <p>
        <label for="code"><?php echo $texts['code']?></label>
        <input name="code" type="text" id="code" placeholder="Insert your code" value="<?php echo $_POST?$_POST['code']:"";?>" />
        <span id="e_code" class="styerror"></span>
      </p>
      <p>
        <label><?php echo $texts['company']?></label>
        <input name="company" type="text" id="company" placeholder="Insert the company" value="<?php echo $_POST?$_POST['company']:"";?>" />
        <span id="e_company" class="styerror"></span>
      </p>
      <p>
        <label for="genere"><?php echo $texts['genere']?></label>
        <select name="genere" id="genere" type="text" placeholder="genere" value="">
          <option value="RPG">RPG</option>
          <option value="MOBA" selected>MOBA</option>
          <option value="Shooter">Shooter</option>
          <option value="Walking Simulator">Walking Simulator</option>
        </select>
      </p>
      <p>
        <label for="consoles"><?php echo $texts['consoles']?></label>
          <input type="checkbox" id="consoles[]" placeholder= "consoles" name="consoles[]" value="Xbox"/>Xbox
          <input type="checkbox" id="consoles[]" placeholder= "consoles" name="consoles[]" value="Nintendo"/>Nintendo
          <input type="checkbox" id="consoles[]" placeholder= "consoles" name="consoles[]" value="PC"/>PC
          <input type="checkbox" id="consoles[]" placeholder= "consoles" name="consoles[]" value="PlayStation"/>PlayStation
      </p>
      <p>
        <label for="price"><?php echo $texts['price']?></label>
        <input name="price" type="number" id="price" placeholder="Insert the price" value="<?php echo $_POST?$_POST['price']:"";?>" />
        <span id="e_price" class="styerror"></span>
      </p>
      <p>
        <label for="daterent"><?php echo $texts['daterent']?></label>
        <input id="daterent" type="text" name="daterent" readonly="readonly" value="<?php echo $_POST?$_POST['daterent']:"";?>" >
        <span id="e_daterent" class="styerror"></span>
      </p>
      <p>
        <label for="votes"><?php echo $texts['votes']?></label>
          0 <input name="votes" type="radio" value="0" checked>
          1 <input name="votes" type="radio" value="1">
          2 <input name="votes" type="radio" value="2">
          3 <input name="votes" type="radio" value="3">
          4 <input name="votes" type="radio" value="4">
          5 <input name="votes" type="radio" value="5">
          6 <input name="votes" type="radio" value="6">
          7 <input name="votes" type="radio" value="7">
          8 <input name="votes" type="radio" value="8">
          9 <input name="votes" type="radio" value="9">
          10 <input name="votes" type="radio" value="10">
      </p>
      <p>
        <label for="comentario"><?php echo $texts['opinion']?></label>
        <textarea cols="30" rows="3" name="opinion" placeholder="I think this game is..." value="<?php echo $_POST?$_POST['opinion']:"";?>" ></textarea>
        <span id="e_opinion" class="styerror"></span>
      </p>
      <p>
        <label for="img"><?php echo $texts['img']?></label>
        <input name="img" type="text" id="img" placeholder="Insert an image" value="<?php echo $_POST?$_POST['img']:"";?>" />
        <span id="e_img" class="styerror">
        </span>
      </p>
        <input class="btn btn-primary btn-outline" type="button" name="create" value="create" onclick="return validate();"/>
                <!--<td><input type="submit" name="create" id="create"/></td>-->
                <td align="right"><a class="btn btn-primary btn-outline" href="index.php?page=controller_games&op=list"><?php echo $texts['back']?></a></td>
    </form>
</div>
