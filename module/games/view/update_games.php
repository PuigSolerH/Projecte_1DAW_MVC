<div id="contenido">
    <form autocomplete="off" method="post" name="update_games" id="update_games" action="index.php?page=controller_games&op=update">
      <td><font color="red">
        <span id="error" class="error">
          <?php
          if(isset($error)){
              print ("<BR><span CLASS='styerror'>" . "* ".$error . "</span><br/>");
          }?>
        </span>
        </font></td>
        <h3><?php echo $texts['update']?></h3>
        <table border='0'>
            <tr>
                <td><?php echo $texts['name']?>: </td>
                <td><input type="text" id="name" name="name" placeholder="name" value="<?php echo $games['name'];?>"/></td>
                    <span id="e_name" class="error">
                    </span>
            </tr>

            <tr>
                <td><?php echo $texts['code']?>: </td>
                <td><input type="text" id="code" name="code" placeholder="code" value="<?php echo $games['code'];?>" readonly/></td>
                <span id="e_code" class="error">
                </span>
            </tr>

            <tr>
                <td><?php echo $texts['company']?>: </td>
                <td><input type="text" id="company" name="company" placeholder="company" value="<?php echo $games['company'];?>"/></td>
                <span id="e_company" class="styerror"></span>
            </tr>

            <tr>
                <td><?php echo $texts['genere']?>: </td>
                <td><select id="genere" name="genere" placeholder="genere">
                    <?php
                        if($games['genere']==="RPG"){
                    ?>
                        <option value="RPG" selected>RPG</option>
                        <option value="MOBA">MOBA</option>
                        <option value="Shooter">Shooter</option>
                        <option value="Walking Simulator">Walking Simulator</option>
                    <?php
                        }elseif($games['genere']==="MOBA"){
                    ?>
                        <option value="RPG" >RPG</option>
                        <option value="MOBA" selected>MOBA</option>
                        <option value="Shooter">Shooter</option>
                        <option value="Walking Simulator">Walking Simulator</option>
                    <?php
                        }elseif($games['genere']==="Shooter"){
                    ?>
                        <option value="RPG" >RPG</option>
                        <option value="MOBA">MOBA</option>
                        <option value="Shooter" selected>Shooter</option>
                        <option value="Walking Simulator">Walking Simulator</option>
                    <?php
                        }else{
                    ?>
                        <option value="RPG" >RPG</option>
                        <option value="MOBA">MOBA</option>
                        <option value="Shooter" >Shooter</option>
                        <option value="Walking Simulator" selected>Walking Simulator</option>
                    <?php
                        }
                    ?>
                    ?>
                    </select></td>
            </tr>

            <tr>
                <td><?php echo $texts['consoles']?>: </td>
                <?php
                    $afi=explode(":", $games['consoles']);
                ?>
                <td>
                    <?php
                        $busca_array=in_array("Xbox", $afi);
                        if($busca_array){
                    ?>
                        <input type="checkbox" id= "consoles[]" name="consoles[]" value="Xbox" checked/>Xbox
                    <?php
                        }else{
                    ?>
                        <input type="checkbox" id= "consoles[]" name="consoles[]" value="Xbox"/>Xbox
                    <?php
                        }
                    ?>
                    <?php
                        $busca_array=in_array("Nintendo", $afi);
                        if($busca_array){
                    ?>
                        <input type="checkbox" id= "consoles[]" name="consoles[]" value="Nintendo" checked/>Nintendo
                    <?php
                        }else{
                    ?>
                        <input type="checkbox" id= "consoles[]" name="consoles[]" value="Nintendo"/>Nintendo
                    <?php
                        }
                    ?>
                    <?php
                        $busca_array=in_array("PC", $afi);
                        if($busca_array){
                    ?>
                        <input type="checkbox" id= "consoles[]" name="consoles[]" value="PC" checked/>PC
                    <?php
                        }else{
                    ?>
                    <input type="checkbox" id= "consoles[]" name="consoles[]" value="PC"/>PC
                    <?php
                        }
                    ?>
                    <?php
                        $busca_array=in_array("PlayStation", $afi);
                        if($busca_array){
                    ?>
                        <input type="checkbox" id= "consoles[]" name="consoles[]" value="PlayStation" checked/>PlayStation</td>
                    <?php
                        }else{
                    ?>
                    <input type="checkbox" id= "consoles[]" name="consoles[]" value="PlayStation"/>PlayStation</td>
                    <?php
                        }
                    ?>
                </td>
            </tr>

            <tr>
                <td><?php echo $texts['price']?>: </td>
                <td><input type="number" id="price" name="price" placeholder="price" value="<?php echo $games['price'];?>"/></td>
                <span id="error_price" class="error">
                </span>
            </tr>

            <tr>
                <td><?php echo $texts['daterent']?>: </td>
                <td><input id="daterent" type="text" name="daterent" placeholder="renting date" value="<?php echo $games['daterent'];?>"/></td>
                <span id="error_daterent" class="error">
                </span>
            </tr>

            <tr>
                <td><?php echo $texts['votes']?>: </td>
                <td>
                  <?php
                      if ($games['votes']==="0"){
                  ?>
                      <input type="radio" id="votes" name="votes" placeholder="0" value="0" checked/>0
                      <input type="radio" id="votes" name="votes" placeholder="1" value="1"/>1
                      <input type="radio" id="votes" name="votes" placeholder="2" value="2"/>2
                      <input type="radio" id="votes" name="votes" placeholder="3" value="3"/>3
                      <input type="radio" id="votes" name="votes" placeholder="4" value="4"/>4
                      <input type="radio" id="votes" name="votes" placeholder="5" value="5"/>5
                      <input type="radio" id="votes" name="votes" placeholder="6" value="6"/>6
                      <input type="radio" id="votes" name="votes" placeholder="7" value="7"/>7
                      <input type="radio" id="votes" name="votes" placeholder="8" value="8"/>8
                      <input type="radio" id="votes" name="votes" placeholder="9" value="9"/>9
                      <input type="radio" id="votes" name="votes" placeholder="10" value="10"/>10
                  <?php
                  }elseif ($games['votes']==="1"){
                  ?>
                      <input type="radio" id="votes" name="votes" placeholder="0" value="0"/>0
                      <input type="radio" id="votes" name="votes" placeholder="1" value="1" checked/>1
                      <input type="radio" id="votes" name="votes" placeholder="2" value="2"/>2
                      <input type="radio" id="votes" name="votes" placeholder="3" value="3"/>3
                      <input type="radio" id="votes" name="votes" placeholder="4" value="4"/>4
                      <input type="radio" id="votes" name="votes" placeholder="5" value="5"/>5
                      <input type="radio" id="votes" name="votes" placeholder="6" value="6"/>6
                      <input type="radio" id="votes" name="votes" placeholder="7" value="7"/>7
                      <input type="radio" id="votes" name="votes" placeholder="8" value="8"/>8
                      <input type="radio" id="votes" name="votes" placeholder="9" value="9"/>9
                      <input type="radio" id="votes" name="votes" placeholder="10" value="10"/>10
                  <?php
                      }elseif ($games['votes']==="2"){
                  ?>
                      <input type="radio" id="votes" name="votes" placeholder="0" value="0"/>0
                      <input type="radio" id="votes" name="votes" placeholder="1" value="1"/>1
                      <input type="radio" id="votes" name="votes" placeholder="2" value="2" checked/>2
                      <input type="radio" id="votes" name="votes" placeholder="3" value="3"/>3
                      <input type="radio" id="votes" name="votes" placeholder="4" value="4"/>4
                      <input type="radio" id="votes" name="votes" placeholder="5" value="5"/>5
                      <input type="radio" id="votes" name="votes" placeholder="6" value="6"/>6
                      <input type="radio" id="votes" name="votes" placeholder="7" value="7"/>7
                      <input type="radio" id="votes" name="votes" placeholder="8" value="8"/>8
                      <input type="radio" id="votes" name="votes" placeholder="9" value="9"/>9
                      <input type="radio" id="votes" name="votes" placeholder="10" value="10"/>10
                  <?php
                      }elseif ($games['votes']==="3"){
                  ?>
                      <input type="radio" id="votes" name="votes" placeholder="0" value="0"/>0
                      <input type="radio" id="votes" name="votes" placeholder="1" value="1"/>1
                      <input type="radio" id="votes" name="votes" placeholder="2" value="2"/>2
                      <input type="radio" id="votes" name="votes" placeholder="3" value="3" checked/>3
                      <input type="radio" id="votes" name="votes" placeholder="4" value="4"/>4
                      <input type="radio" id="votes" name="votes" placeholder="5" value="5"/>5
                      <input type="radio" id="votes" name="votes" placeholder="6" value="6"/>6
                      <input type="radio" id="votes" name="votes" placeholder="7" value="7"/>7
                      <input type="radio" id="votes" name="votes" placeholder="8" value="8"/>8
                      <input type="radio" id="votes" name="votes" placeholder="9" value="9"/>9
                      <input type="radio" id="votes" name="votes" placeholder="10" value="10"/>10
                  <?php
                      }elseif ($games['votes']==="4"){
                  ?>
                      <input type="radio" id="votes" name="votes" placeholder="0" value="0"/>0
                      <input type="radio" id="votes" name="votes" placeholder="1" value="1"/>1
                      <input type="radio" id="votes" name="votes" placeholder="2" value="2"/>2
                      <input type="radio" id="votes" name="votes" placeholder="3" value="3"/>3
                      <input type="radio" id="votes" name="votes" placeholder="4" value="4" checked/>4
                      <input type="radio" id="votes" name="votes" placeholder="5" value="5"/>5
                      <input type="radio" id="votes" name="votes" placeholder="6" value="6"/>6
                      <input type="radio" id="votes" name="votes" placeholder="7" value="7"/>7
                      <input type="radio" id="votes" name="votes" placeholder="8" value="8"/>8
                      <input type="radio" id="votes" name="votes" placeholder="9" value="9"/>9
                      <input type="radio" id="votes" name="votes" placeholder="10" value="10"/>10
                  <?php
                      }elseif ($games['votes']==="5"){
                  ?>
                      <input type="radio" id="votes" name="votes" placeholder="0" value="0"/>0
                      <input type="radio" id="votes" name="votes" placeholder="1" value="1"/>1
                      <input type="radio" id="votes" name="votes" placeholder="2" value="2"/>2
                      <input type="radio" id="votes" name="votes" placeholder="3" value="3"/>3
                      <input type="radio" id="votes" name="votes" placeholder="4" value="4"/>4
                      <input type="radio" id="votes" name="votes" placeholder="5" value="5" checked/>5
                      <input type="radio" id="votes" name="votes" placeholder="6" value="6"/>6
                      <input type="radio" id="votes" name="votes" placeholder="7" value="7"/>7
                      <input type="radio" id="votes" name="votes" placeholder="8" value="8"/>8
                      <input type="radio" id="votes" name="votes" placeholder="9" value="9"/>9
                      <input type="radio" id="votes" name="votes" placeholder="10" value="10"/>10
                  <?php
                      }elseif ($games['votes']==="6"){
                  ?>
                      <input type="radio" id="votes" name="votes" placeholder="0" value="0"/>0
                      <input type="radio" id="votes" name="votes" placeholder="1" value="1"/>1
                      <input type="radio" id="votes" name="votes" placeholder="2" value="2"/>2
                      <input type="radio" id="votes" name="votes" placeholder="3" value="3"/>3
                      <input type="radio" id="votes" name="votes" placeholder="4" value="4"/>4
                      <input type="radio" id="votes" name="votes" placeholder="5" value="5"/>5
                      <input type="radio" id="votes" name="votes" placeholder="6" value="6" checked/>6
                      <input type="radio" id="votes" name="votes" placeholder="7" value="7"/>7
                      <input type="radio" id="votes" name="votes" placeholder="8" value="8"/>8
                      <input type="radio" id="votes" name="votes" placeholder="9" value="9"/>9
                      <input type="radio" id="votes" name="votes" placeholder="10" value="10"/>10
                  <?php
                      }elseif ($games['votes']==="7"){
                  ?>
                      <input type="radio" id="votes" name="votes" placeholder="0" value="0"/>0
                      <input type="radio" id="votes" name="votes" placeholder="1" value="1"/>1
                      <input type="radio" id="votes" name="votes" placeholder="2" value="2"/>2
                      <input type="radio" id="votes" name="votes" placeholder="3" value="3"/>3
                      <input type="radio" id="votes" name="votes" placeholder="4" value="4"/>4
                      <input type="radio" id="votes" name="votes" placeholder="5" value="5"/>5
                      <input type="radio" id="votes" name="votes" placeholder="6" value="6"/>6
                      <input type="radio" id="votes" name="votes" placeholder="7" value="7" checked/>7
                      <input type="radio" id="votes" name="votes" placeholder="8" value="8"/>8
                      <input type="radio" id="votes" name="votes" placeholder="9" value="9"/>9
                      <input type="radio" id="votes" name="votes" placeholder="10" value="10"/>10
                  <?php
                      }elseif ($games['votes']==="8"){
                  ?>
                      <input type="radio" id="votes" name="votes" placeholder="0" value="0"/>0
                      <input type="radio" id="votes" name="votes" placeholder="1" value="1"/>1
                      <input type="radio" id="votes" name="votes" placeholder="2" value="2"/>2
                      <input type="radio" id="votes" name="votes" placeholder="3" value="3"/>3
                      <input type="radio" id="votes" name="votes" placeholder="4" value="4"/>4
                      <input type="radio" id="votes" name="votes" placeholder="5" value="5"/>5
                      <input type="radio" id="votes" name="votes" placeholder="6" value="6"/>6
                      <input type="radio" id="votes" name="votes" placeholder="7" value="7"/>7
                      <input type="radio" id="votes" name="votes" placeholder="8" value="8" checked/>8
                      <input type="radio" id="votes" name="votes" placeholder="9" value="9"/>9
                      <input type="radio" id="votes" name="votes" placeholder="10" value="10"/>10
                  <?php
                      }elseif ($games['votes']==="9"){
                  ?>
                      <input type="radio" id="votes" name="votes" placeholder="0" value="0"/>0
                      <input type="radio" id="votes" name="votes" placeholder="1" value="1"/>1
                      <input type="radio" id="votes" name="votes" placeholder="2" value="2"/>2
                      <input type="radio" id="votes" name="votes" placeholder="3" value="3"/>3
                      <input type="radio" id="votes" name="votes" placeholder="4" value="4"/>4
                      <input type="radio" id="votes" name="votes" placeholder="5" value="5"/>5
                      <input type="radio" id="votes" name="votes" placeholder="6" value="6"/>6
                      <input type="radio" id="votes" name="votes" placeholder="7" value="7"/>7
                      <input type="radio" id="votes" name="votes" placeholder="8" value="8"/>8
                      <input type="radio" id="votes" name="votes" placeholder="9" value="9" checked/>9
                      <input type="radio" id="votes" name="votes" placeholder="10" value="10"/>10
                  <?php
                      }elseif ($games['votes']==="10"){
                  ?>
                      <input type="radio" id="votes" name="votes" placeholder="0" value="0"/>0
                      <input type="radio" id="votes" name="votes" placeholder="1" value="1"/>1
                      <input type="radio" id="votes" name="votes" placeholder="2" value="2"/>2
                      <input type="radio" id="votes" name="votes" placeholder="3" value="3"/>3
                      <input type="radio" id="votes" name="votes" placeholder="4" value="4"/>4
                      <input type="radio" id="votes" name="votes" placeholder="5" value="5"/>5
                      <input type="radio" id="votes" name="votes" placeholder="6" value="6"/>6
                      <input type="radio" id="votes" name="votes" placeholder="7" value="7"/>7
                      <input type="radio" id="votes" name="votes" placeholder="8" value="8"/>8
                      <input type="radio" id="votes" name="votes" placeholder="9" value="9"/>9
                      <input type="radio" id="votes" name="votes" placeholder="10" value="10" checked/>10
                  <?php
                    }
                  ?>
                </td>
            </tr>

            <tr>
                <td><?php echo $texts['opinion']?>: </td>
                <td><textarea cols="30" rows="5" id="opinion" name="opinion" placeholder="opinion"><?php echo $games['opinion'];?></textarea></td>
                <span id="e_opinion" class="error">
                </span>
            </tr>
            <tr>
                <td>IMG: </td>
                <td><input type="text" id="img" name="img" placeholder="img" value="<?php echo $games['img'];?>"/></td>
                <span id="e_img" class="error">
                </span>
            </tr>
            <tr>
              <!--<td><input type="button" name="update" value="update" onclick="return validate();"/></td>-->
                <td><button class="btn btn-primary btn-outline" name="update" id="update" onclick="return validate();"><?php echo $texts['update']?></button></td>
                <td align="right"><a class="btn btn-primary btn-outline" href="index.php?page=controller_games&op=list"><?php echo $texts['back']?></a></td>
            </tr>
        </table>
    </form>
</div>
