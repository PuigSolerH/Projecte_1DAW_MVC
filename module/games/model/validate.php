<?php
    function validate(){
      $error='';
      $data='';
  		$filtro = array(
  			'name' => array(
  				'filter'=>FILTER_VALIDATE_REGEXP,
  				'options'=>array('regexp'=>'/[0-9A-Za-z]{3,20}$/')
  			),

  			'code' => array(
  				'filter'=>FILTER_VALIDATE_REGEXP,
  				'options'=>array('regexp'=>'/([0-9]{8}[A-Z])$/')
  			),

  			'company' => array(
  				'filter'=>FILTER_VALIDATE_REGEXP,
  				'options'=>array('regexp'=>'/^[a-zA-Z0-9]{1,50}$/')
  			),

  			'price' => array(
  				'filter'=>FILTER_VALIDATE_REGEXP,
  				'options'=>array('regexp'=>'/[0-9]+(\.[0-9]{2})?$/')
  			),

  			'opinion' => array(
  				'filter' => FILTER_VALIDATE_REGEXP,
  				'options' => array('regexp' => '/[0-9A-Za-z]{20,320}$/')
  			),

        'img' => array(
          'filter' => FILTER_VALIDATE_REGEXP,
          'options' => array('regexp' => '/[0-9A-Za-z]+(\.[a-z]{3,4})?$/')
        ),
  		);
  	 	$resultado=filter_input_array(INPUT_POST,$filtro);

          if (!$resultado['name']) {
              $error= 'Name must have more than 3 characters and less than 20';
          }
          elseif (!$resultado['code']) {
              $error= 'Invalid Code. Ex: 12345678A';
          }
          elseif (!$resultado['company']) {
              $error= 'Invalid company';
          }
          elseif (!$resultado['price']) {
              $error= 'Invalid price value. Ex: X.XX';
          }
          elseif (!$resultado['opinion']) {
              $error= 'Opinion must have more than 20 characters and less than 320';
          }
          elseif (!$resultado['img']) {
              $error= 'Invalid image format. Ex: games1.jpg';
          }
          else {
              return $return = array('resultado' => true , 'error' => $error , 'data' => $resultado);
          };
          return $return = array('resultado' => false , 'error' => $error , 'data' => $resultado);
      };

?>
