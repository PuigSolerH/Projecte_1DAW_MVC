function validate() {
	if (document.formulario.name.value.length===0){
	    document.getElementById('e_name').innerHTML = "Introduce the game name";
	    document.formulario.name.focus();
	    return 0;
    }
    document.getElementById('e_name').innerHTML = "";

    if (document.formulario.code.value.length===0){
	    document.getElementById('e_code').innerHTML = "Introduce the game code";
	    document.formulario.code.focus();
	    return 0;
    }
    document.getElementById('e_code').innerHTML = "";

    if (document.formulario.company.value.length===0){
	    document.getElementById('e_company').innerHTML = "Introduce the game company";
	    document.formulario.company.focus();
	    return 0;
    }
    document.getElementById('e_company').innerHTML = "";

		if (document.formulario.price.value.length===0){
	    document.getElementById('e_price').innerHTML = "Introduce the game price";
	    document.formulario.price.focus();
	    return 0;
    }
    document.getElementById('e_price').innerHTML = "";

    if (document.formulario.daterent.value.length===0){
	    document.getElementById('e_daterent').innerHTML = "Introduce the renting date";
	    document.formulario.daterent.focus();
	    return 0;
    }
    document.getElementById('e_daterent').innerHTML = "";

		if (document.formulario.opinion.value.length===0){
	    document.getElementById('e_opinion').innerHTML = "Introduce an opinion";
	    document.formulario.opinion.focus();
	    return 0;
    }
    document.getElementById('e_opinion').innerHTML = "";

		if (document.formulario.img.value.length===0){
			document.getElementById('e_img').innerHTML = "Introduce an opinion";
			document.formulario.img.focus();
			return 0;
		}
		document.getElementById('e_img').innerHTML = "";

	document.formulario.submit();
	document.formulario.action="index.php?page=controller_games";
}

$(document).ready(function () {
        $('.list_game').click(function () {
            var id = this.getAttribute('id');
            //alert(id);

			$.get("module/games/controller/controller_games.php?op=read_modal&modal=" + id, function (data, status) {
				//
                var json = JSON.parse(data);
                console.log(json);

                if(json === 'error') {
                    //console.log(json);
                    //pintar 503
    			    window.location.href='index.php?page=503';
                }else{
                    console.log(json.games);
                    $("#name").html(json.name);
                    $("#code").html(json.code);
                    $("#company").html(json.company);
                    $("#genere").html(json.genere);
                    $("#consoles").html(json.consoles);
										$("#price").html(json.price);
                    $("#daterent").html(json.daterent);
                    $("#votes").html(json.votes);
										$("#opinion").html(json.opinion);
										// $("#img").html(json.img);

                    $("#details_games").show();
                    $("#myModal").dialog({
                        width: 850, //<!-- ------------- ancho de la ventana -->
                        height: 500, //<!--  ------------- altura de la ventana -->
                        show: "scale", <!-- ----------- animación de la ventana al aparecer -->
                        hide: "scale", <!-- ----------- animación al cerrar la ventana -->
                        resizable: "false", //<!-- ------ fija o redimensionable si ponemos este valor a "true" -->
                        position: "down",<!--  ------ posicion de la ventana en la pantalla (left, top, right...) -->
                        modal: "true", //<!-- ------------ si esta en true bloquea el contenido de la web mientras la ventana esta activa (muy elegante) -->
                        buttons: {
                            Ok: function () {
                                $(this).dialog("close");
                            }
                        },
                        show: {
                            effect: "blind",
                            duration: 1000
                        },
                        hide: {
                            effect: "explode",
                            duration: 1000
                        }
                    });
                }//end-else
            });
        });
	});
