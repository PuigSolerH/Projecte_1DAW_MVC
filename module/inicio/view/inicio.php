<div id="fh5co-work-section" class="fh5co-light-grey-section">
  <div class="container">
      <div class="row">
          <div class="form-group col-md-4">&nbsp;
              <select class="form-control" id="country"></select>
          </div>
          <div class="form-group col-md-4">&nbsp;
              <select class="form-control" id="genere"></select>
          </div>
          </br>
          <div class="form-group col-md-4">&nbsp;
            <button class="btn btn-default" id="list_json" type="submit"><span class="glyphicon glyphicon-list"></span> MORE INFO</button>
          </div>
          <div id="results_json"></div>
      </div>
    </br>
      <div class="row">
        <div class="form-group col-md-4">&nbsp;
            <form id="autocomplete_list">
               <input type="text" size="50" id="codes" name="name" />
               <div id="suggestion"></div>
            </form>
        </div>
      </br>
        <div class="form-group col-md-4">&nbsp;
          <button class="btn btn-default" id="list_selected" type="submit"><span class="glyphicon glyphicon-list"></span> MORE INFO</button>
        </div>
      </br></br></br>
        <div id="results"></div>
      </div>
  </div>
</div>
<aside id="fh5co-hero" class="js-fullheight">
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">

      <div class="item active">
        <img src="assets/images/g1.jpg" alt="Games1" style="width:100%;">
        <div class="carousel-caption">
          <!-- <h3>Los Angeles</h3>
          <p>LA is always so much fun!</p> -->
        </div>
      </div>

      <div class="item">
        <img src="assets/images/g2.jpg" alt="Games2" style="width:100%;">
        <div class="carousel-caption">
          <!-- <h3>Chicago</h3>
          <p>Thank you, Chicago!</p> -->
        </div>
      </div>

      <div class="item">
        <img src="assets/images/g3.jpg" alt="Games3" style="width:100%;">
        <div class="carousel-caption">
          <!-- <h3>New York</h3>
          <p>We love the Big Apple!</p> -->
        </div>
      </div>

    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</aside> 
