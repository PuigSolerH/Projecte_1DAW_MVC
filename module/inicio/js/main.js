var generes;
var countries;

var searchIntoJson = function (obj, column, value) {
    var results = [];
    var valueField;
    var searchField = column;
    for (var i = 0 ; i < obj.length ; i++) {
        valueField = obj[i][searchField].toString();
        if (valueField === value) {
            results.push(obj[i]);
        }
    }
    return results;
};

var loadCountries = function () {
    $("#country").empty();
    $("#country").append('<option value="" selected="selected"></option>');
    $.each(countries, function (i, valor) {
        $("#country").append("<option value='" + valor.country_id + "'>" + valor.country_name + "</option>");
    });
};

var loadGeneres = function (country_id) {
    var generesDepto = searchIntoJson(generes, "country_id", country_id);
    $("#genere").empty();
    $("#genere").append('<option value="" selected="selected"></option>');
    $.each(generesDepto, function (i, valor) {
        $("#genere").append('<option value="' + valor.genereId + '">' + valor.genere + '</option>');
    });
};

$(document).ready(function () {
    $.getJSON("module/inicio/data/countries.json", function (data) {
        countries = data;
    });

    $.getJSON("module/inicio/data/generes.json", function (data) {
        generes = data;
        setTimeout(function () {
            if (generes !== undefined) {
                loadCountries();
            }
        }, 2000);
    });

    $("#country").change(function () {
        var country_id = $("#country").val();
        loadGeneres(country_id);
    });
});

$(document).ready(function() {
    //Al escribr dentro del input con id="service"
    $('#codes').keyup(function(){
        //Obtenemos el value del input
        var codes = $(this).val();
        var dataString = {code:codes};

        //Le pasamos el valor del input al ajax
        $.ajax({
            type: "POST",
            url: "module/inicio/controller/controller_inicio.php?op=autocomplete",
            data: dataString,
            success: function(data) {
                //Escribimos las sugerencias que nos manda la consulta
                $('#suggestion').fadeIn(1000).html(data);
                // console.log(data);
                //Al hacer click en algua de las sugerencias
                $('.suggest-element').on('click', function(){
                    //Obtenemos la id unica de la sugerencia pulsada
                    var id = $(this).attr('id');
                    //Editamos el valor del input con data de la sugerencia pulsada
                    $('#codes').val($('#'+id).attr('data'));
                    //Hacemos desaparecer el resto de sugerencias
                    $('#suggestion').fadeOut(1000);
                });
            }
        });
    });
});

$(document).ready(function() {
  $('button#list_selected').click(function(event){
    // alert("HOLA");
    // event.preventDefault(); //prevent default action
    var post_url = "module/inicio/controller/controller_inicio.php?op=list_name";
    // console.log(post_url);
    var  form_data = "name="+$('#codes').val();
    // console.log(form_data);
    var $div=$("div#results");
    $div.html("");
    $.post(post_url, form_data, function(response) {
      // alert("HOLA");
      // console.log(response);
      var json = JSON.parse(response);
      console.log(json);

      $.each(json,function(index ,object){
       var $row = $('<div class="col-md-4">' +
                      '<div class="item-grid text-center">' +
                      '<div class="image"><img src="assets/images/' + object.img + '"></img>' +
                      '</div>' +
                      '<div class="v-align">' +
                      '<div class="v-align-middle">' +
                      '<h3 class="title">' + object.name + '</h3>' +
                      '<h5 class="category">' + object.genere + '</h5>' +
                      '<h5 class="category">' + object.price + '</h5>' +
                      '<a class="btn btn-primary btn-outline" href="index.php?page=controller_dummies&op=read&id=' + object.code + '">Read</a>' +
                      '</div>' +
                      '</div>' +
                      '</div>' +
                      '</div>').appendTo($div);
      })
    });
  });
});


$(document).ready(function() {
  $('button#list_json').click(function(event){
    // alert("HOLA");
    // event.preventDefault(); //prevent default action
    var post_url = "module/inicio/controller/controller_inicio.php?op=list_json";
    // console.log(post_url);
    var  form_data = "genere="+$('#genere').val();
    // console.log(form_data);
    var $div=$("div#results_json");
    $div.html("");
    $.post(post_url, form_data, function(response) {
      // alert("HOLA");
      // console.log(response);
      var json = JSON.parse(response);
      // console.log(json);

      $.each(json,function(index ,object){
       var $row = $('<div class="col-md-4">' +
                      '<div class="item-grid text-center">' +
                      '<div class="image"><img src="assets/images/' + object.img + '"></img>' +
                      '</div>' +
                      '<div class="v-align">' +
                      '<div class="v-align-middle">' +
                      '<h3 class="title">' + object.name + '</h3>' +
                      '<h5 class="category">' + object.genere + '</h5>' +
                      '<h5 class="category">' + object.price + '</h5>' +
                      '<a class="btn btn-primary btn-outline" href="index.php?page=controller_dummies&op=read&id=' + object.code + '">Read</a>' +
                      '</div>' +
                      '</div>' +
                      '</div>' +
                      '</div>').appendTo($div);
      })
    });
  });
});
