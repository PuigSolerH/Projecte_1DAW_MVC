<?php
	session_start();
		require('assets/languages/lang.php');
		if (isset($_POST["lang"])){
		        $lang = $_POST["lang"];
		        $_SESSION ["lang"] = $lang;
		    }

		    if(isset($_SESSION['lang'])){
		        include ('assets/languages/lang.php');
		    }else{
		        include ('assets/languages/es.php');
		    }

?>
<?php include("assets/inc/top_page.php"); ?>
<div id="fh5co-page">
    <header id="fh5co-header" role="banner">
    	<?php
			// $now = time();
			// if($now > $_SESSION['expire']) {
			// session_destroy();
			// echo "Su sesion a terminado,<a href='login.html'>Necesita Hacer Login</a>";
			// exit;
			// }

    	   if(isset($_SESSION['user_logged'])){
					 // print_r($_SESSION);
					 // die();
					 $now = time();
					 if($now > $_SESSION['expire']) {
					 session_destroy();
					 // print_r("SU SESION HA CADUCADO");
					 exit;
				 	 }
					 if(($_SESSION['user_logged']->type)=="admin"){
						 include("assets/inc/header.php");
					 }else if(($_SESSION['user_logged']->type)=="client"){
						 include("assets/inc/header_user.php");
					 }else{
						 include("assets/inc/header_nonuser.php");
					 }
				 }else{
					 include("assets/inc/header_nonuser.php");
				 }
    	?>
		</header>
    <div id="">
    	<?php
		    include("assets/inc/pages.php");
		?>
        <br style="clear:both;" />
    </div>
    <footer id="fh5co-footer" role="contentinfo">
	    <?php
	        include("assets/inc/footer.php");
	    ?>
    </footer>
</div>
<?php
    include("assets/inc/bottom_page.php");
?>
